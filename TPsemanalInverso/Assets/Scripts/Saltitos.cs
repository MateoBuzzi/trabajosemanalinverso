﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saltitos : MonoBehaviour
{
    private Rigidbody rb;
    public float salto = 2;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

     void update() {

        if (Input.GetKeyDown(KeyCode.W))
        {
            rb.velocity= Vector2.up*salto;
        }

    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            Destroy(this.gameObject);
        }
    }

    


}
