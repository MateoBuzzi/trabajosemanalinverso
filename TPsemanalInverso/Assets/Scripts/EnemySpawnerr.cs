﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerr : MonoBehaviour
{
    public float maxTime = 1;
    private float timer = 0;
    public GameObject Enemy;
    public float Altura;

    private void Start()
    {
        GameObject newenemy = Instantiate(Enemy);
        newenemy.transform.position = transform.position + new Vector3(0, Random.Range(-Altura, Altura), 0);

       }

    private void Update()
    {
        if (timer > maxTime)
        {
            GameObject newenemy = Instantiate(Enemy);
            newenemy.transform.position = transform.position + new Vector3(0, Random.Range(-Altura, Altura), 0);
            Destroy(newenemy, 15);
            timer = 0;

        }
        timer += Time.deltaTime;
    }
}
